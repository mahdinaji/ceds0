﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEDatastructure0.Core
{
    class Consts
    {
        public static char[] Numbers = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        public static char[] Vars = new char[] {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        };
        public static char[] Opts = new char[] { '^', '*', '/', '+', '-' };

        public static string[] ExpressionTypes = new string[] { "number", "variable", "term", "Not Constructed" };

        public static Dictionary<string, string> Constants = new Dictionary<string, string>()
        {
            { "PI", Math.PI.ToString().Replace("/", ".") },
            { "NP", Math.E.ToString().Replace("/", ".") }
        };
    }
}
