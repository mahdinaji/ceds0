﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEDatastructure0.Core
{
    class Expression
    {
        #region properties

        public bool isNumber = false;
        private double number = 0;
        public double Number { get { return this.number; } }
        private bool isFloatShifted = false;
        private double floatShiftM = 1;

        public bool isVar = false;
        private string varName = "";
        public string VariableName { get { return this.varName; } }

        private bool isPolynomial = false;
        public List<Expression> terms = new List<Expression>();

        private bool isConstructed = false;
        public string currentType = "Not Constructed";

        public bool hasOperator = false;
        public Operator currentOperator;

        private Dictionary<string, double> currentParams = new Dictionary<string, double>() { };

        #endregion


        #region construtors

        public Expression(double defaultfloat)
        {
            this.isNumber = true;
            this.isVar = false;
            this.number = defaultfloat;
            this.isConstructed = true;
            this.currentType = "number";
        }

        public Expression(string defaultVarName)
        {
            this.isNumber = false;
            this.isVar = true;
            this.varName = defaultVarName;
            if (this.varName.Length != 1)
            {
                throw new Exception("variable expression can be 1 characters long, current var name: " + this.varName);
            }
            // TODO: accept multi vars
            this.isConstructed = true;
            this.currentType = "variable";
        }

        public Expression(string value, string type)
        {
            switch (type)
            {
                case "number":
                    double currentVal = 0;
                    try
                    {
                        currentVal = double.Parse(value);
                    }
                    catch (Exception exc)
                    {
                        throw new Exception("cannot Convert '" + value + "' to float32,\n\nExceptionMsg:" + exc.Message);
                    }
                    this.isNumber = true;
                    this.number = currentVal;
                    this.isVar = false;
                    this.isConstructed = true;
                    this.currentType = "number";
                    break;
                case "variable":
                    if (value.Length != 1)
                    {
                        throw new Exception("variable expression can be 1 characters long, current var name: " + value);
                        // TODO: accept multi vars like above
                    }
                    this.isNumber = false;
                    this.isPolynomial = false;
                    this.isVar = true;
                    this.varName = value;
                    this.isConstructed = true;
                    this.currentType = "variable";
                    break;
                default:
                    throw new Exception("Unknown expression type, currentExpressionType:" + type);
            }
        }

        public Expression()
        {
            this.isNumber = false;
            this.isVar = false;
            this.number = 0;
            this.isConstructed = false;
            this.currentType = "Not Constructed";
        }

        public Expression(Expression oldOne)
        {
            // make a deep clone of current expression
            this.isNumber = oldOne.isNumber;
            this.number = oldOne.number;
            this.isVar = oldOne.isVar;
            this.varName = oldOne.varName;
            this.isPolynomial = oldOne.isPolynomial;
            this.terms = new List<Expression>();
            if (oldOne.terms != null)
            {
                oldOne.terms.ForEach(new Action<Expression>((Expression element) =>
                {
                    this.terms.Add(new Expression(element));
                }));
            }
            this.isConstructed = oldOne.isConstructed;
            this.currentType = oldOne.currentType;
            this.hasOperator = oldOne.hasOperator;
            this.currentOperator = new Operator(oldOne.currentOperator);
        }

        public Expression(List<Expression> floaterms)
        {
            this.terms = floaterms;
            this.isPolynomial = true;
            this.isNumber = false;
            this.isVar = false;
            this.isConstructed = true;
        }

        #endregion


        public void Analyze(string Raw, Dictionary<string, double> ParamsList)
        {
            Raw = Raw.Replace(" ", "");
            Raw = Raw.Replace("\n", "");
            foreach (var CONST in Consts.Constants)
            {
                Raw = Raw.Replace(CONST.Key, CONST.Value);
            }
            this.Analyze(Raw, ParamsList, '!');
        }

        public int Analyze(string Raw, Dictionary<string, double> ParamsList, char endChar)
        {
            if (this.isConstructed)
            {
                throw new Exception("Expression is already constructed");
            }

            Expression lastExp = new Expression(); // first expression must be 'not constructed'
            Operator lastOperator = null;

            int i = 0;
            for (i = 0; i < Raw.Length; i++)
            {
                char currentChar = Raw[i];
                if (currentChar == ']' || currentChar == ')')
                {
                    if (lastOperator != null)
                    {
                        throw new Exception("no expression between operator and '" + endChar.ToString() + "' at index:" + (i).ToString() + " from '" + Raw + "'");
                    }
                    this.terms.Add(new Expression(lastExp));
                    this.isConstructed = true;
                    this.currentType = "term";
                    this.isPolynomial = true;
                    return i;
                }
                string rest = Raw.Substring(i, Raw.Length-i);
                bool isFunction = false;
                foreach(var functionName in Functions.Funcs.Keys)
                {
                    if (isFunction)
                        continue;
                    if(rest.StartsWith(functionName))
                    {
                        i += functionName.Length;
                        string restOfRaw = Raw.Substring(i + 1, Raw.Length - i - 1);
                        Expression innerExpression = new Expression();
                        try
                        {
                            i = innerExpression.Analyze(restOfRaw, ParamsList, ')') + i + 1;
                            innerExpression = innerExpression.Calculate();
                        }
                        catch (Exception ert)
                        {
                            string msg = string.Join("\n\t", ert.Message.Split('\n'));
                            throw new Exception("Exception at analyzing function params at index:" + (i).ToString() + " from '" + Raw + "'\n\twith: " + msg);
                        }
                        innerExpression = Functions.Funcs[functionName](innerExpression).Calculate();
                        innerExpression.currentOperator = lastOperator;
                        lastExp = innerExpression;
                        lastOperator = null;
                        isFunction = true;
                    }
                }
                if (isFunction)
                    continue;
                if (currentChar == '[')
                {
                    string restOfRaw = Raw.Substring(i + 1, Raw.Length - i - 1);
                    Expression innerExpression = new Expression();
                    try
                    {
                        i = innerExpression.Analyze(restOfRaw, ParamsList, ']') + 1 + i;
                        innerExpression = innerExpression.Calculate();
                    }
                    catch (Exception ert) 
                    {
                        string msg = string.Join("\n\t", ert.Message.Split('\n'));
                        throw new Exception("Exception at analyzing [] at index:" + (i).ToString() + " from '" + Raw + "'\n\twith: " + msg);
                    }
                    innerExpression.currentOperator = lastOperator;
                    lastExp = innerExpression;
                    lastOperator = null;
                    continue;
                }
                else if(currentChar == '.')
                {
                    lastExp.isFloatShifted = true;
                }
                else if (currentChar == '(')
                {
                    string restOfRaw = Raw.Substring(i + 1, Raw.Length - i - 1);
                    Expression innerExpression = new Expression();
                    try
                    {
                        i = innerExpression.Analyze(restOfRaw, ParamsList, ')') + i + 1;
                        innerExpression = innerExpression.Calculate();
                    }
                    catch (Exception ert)
                    {
                        string msg = string.Join("\n\t", ert.Message.Split('\n'));
                        throw new Exception("Exception at analyzing () at index:" + (i).ToString() + " from '" + Raw + "'\n\twith: " + msg);
                    }
                    innerExpression.currentOperator = lastOperator;
                    lastExp = innerExpression;
                    lastOperator = null;
                    continue;
                }
                else if (Consts.Numbers.Contains(currentChar))
                {
                    if (lastOperator != null)
                    {
                        lastExp = new Expression(currentChar.ToString(), "number");
                        lastExp.currentOperator = lastOperator;
                        lastOperator = null;
                        continue;
                    }
                    if (lastExp.currentType == "number")
                    {
                        lastExp.PushToNumber(currentChar);
                    }
                    else if (lastExp.currentType == "variable")
                    {
                        throw new Exception("no operator between variable and number at index:" + (i).ToString() + " from '" + Raw + "'");
                    }
                    else if (lastExp.currentType == "Not Constructed")
                    {
                        lastExp = new Expression(currentChar.ToString(), "number");
                    }
                }
                else if (Consts.Vars.Contains(currentChar))
                {
                    if (lastOperator != null)
                    {
                        if (ParamsList.Keys.Contains<string>(currentChar.ToString()))
                        {
                            lastExp = new Expression(ParamsList[currentChar.ToString()]);
                            lastExp.varName = currentChar.ToString(); // just in case :D :D :D
                        }
                        else
                        {
                            lastExp = new Expression(currentChar.ToString(), "variable");
                        }
                        lastExp.currentOperator = lastOperator;
                        lastOperator = null;
                        continue;
                    }
                    if (lastExp.currentType == "number")
                    {
                        this.terms.Add(new Expression(lastExp));
                        if (ParamsList.Keys.Contains<string>(currentChar.ToString()))
                        {
                            lastExp = new Expression(ParamsList[currentChar.ToString()]);
                            lastExp.varName = currentChar.ToString();
                        }
                        else
                        {
                            lastExp = new Expression(currentChar.ToString(), "variable");
                        }
                        lastExp.currentOperator = new Operator('*');
                    }
                    else if (lastExp.currentType == "variable")
                    {
                        this.terms.Add(new Expression(lastExp));
                        if (ParamsList.Keys.Contains<string>(currentChar.ToString()))
                        {
                            lastExp = new Expression(ParamsList[currentChar.ToString()]);
                            lastExp.varName = currentChar.ToString();
                        }
                        else
                        {
                            lastExp = new Expression(currentChar.ToString(), "variable");
                        }
                        lastExp.currentOperator = new Operator('*');
                    }
                    else if (lastExp.currentType == "Not Constructed")
                    {
                        if (ParamsList.Keys.Contains<string>(currentChar.ToString()))
                        {
                            lastExp = new Expression(ParamsList[currentChar.ToString()]);
                            lastExp.varName = currentChar.ToString();
                        }
                        else
                        {
                            lastExp = new Expression(currentChar.ToString(), "variable");
                        }
                    }
                }
                else if (Consts.Opts.Contains(currentChar))
                {
                    if (lastOperator != null)
                    {
                        throw new Exception("no expression between two operator at index:" + (i).ToString() + " from '" + Raw + "'");
                    }
                    if (lastExp.currentType == "number" || lastExp.currentType == "variable" || lastExp.currentType == "term")
                    {
                        this.terms.Add(new Expression(lastExp));
                        lastOperator = new Operator(currentChar);
                    }
                    else if (lastExp.currentType == "Not Constructed")
                    {
                        lastOperator = new Operator(currentChar);
                        lastExp = lastOperator.defaultExpression();
                        this.terms.Add(new Expression(lastExp));
                    }
                }
            }
            this.terms.Add(new Expression(lastExp));

            this.isConstructed = true;
            this.currentType = "term";
            this.isPolynomial = true;
            return i-1;
        }

        public double PushToNumber(char num)
        {
            if(!this.isConstructed)
            {
                throw new Exception("Expression not constructed yet");
            }
            if (!this.isNumber)
            {
                throw new Exception("Wrong expression type, currentType: " + this.varName);
            }
            double currentNum = 0;
            try
            {
                currentNum = double.Parse(num.ToString());
            }
            catch (Exception exc)
            {
                throw new Exception("cannot push number('" + num.ToString() + "') to float32,\n\nExceptionMsg:" + exc.Message);
            }
            if (this.isFloatShifted)
            {
                floatShiftM /= 10;
                currentNum *= floatShiftM;
            }
            else
            {
                this.number *= 10;
            }
            this.number += currentNum;
            return this.number;
        }

        public Expression Calculate()
        {
            if (this.isPolynomial)
            {
                for (int i = 0; i < Consts.Opts.Length; i++)
                {
                    char currentOrder = Consts.Opts[i];
                    for (int j = 1; j < this.terms.Count; j++)
                    {
                        if (this.terms[j].currentOperator.TypeId == currentOrder)
                        {
                            Expression currentOrderAnswer = this.terms[j].Operate(this.terms[j - 1]);
                            currentOrderAnswer.currentOperator = this.terms[j - 1].currentOperator;
                            this.terms[j] = currentOrderAnswer;
                            this.terms.RemoveAt(j - 1);
                            j--;
                        }
                    }
                }
                return this.terms[0].Calculate();
            }
            else
            {
                return this;
            }
        }

        public Expression Operate(Expression lastExpression)
        {
            Expression Answer = new Expression();
            if (!this.isConstructed)
            {
                throw new Exception("Expression not constructed yet");
            }
            if (this.isPolynomial)
            {
                Expression PolynomialAnswer = this.Calculate();
                if (PolynomialAnswer.isNumber)
                {
                    Answer = this.currentOperator.Operate(PolynomialAnswer, lastExpression);
                }
                else
                {
                    Answer = new Expression(new List<Expression> { lastExpression, PolynomialAnswer });
                }
            }
            else
            {
                if (this.isNumber)
                {
                    Answer = this.currentOperator.Operate(this, lastExpression);
                }
                else
                {
                    Answer = new Expression(new List<Expression> { lastExpression, this });
                }
            }
            return Answer;
        }

        public string ToString()
        {
            string Result = "";
            if (this.isPolynomial)
            {
                for (int i = 0; i < this.terms.Count; i++)
                {
                    Result += this.terms[i].ToString();
                }
                return "(" + Result + ")";
            }
            else
            {
                if (this.isNumber)
                {
                    Result += this.currentOperator != null && this.currentOperator.TypeId != '!' ? this.currentOperator.TypeId.ToString() : "";
                    return Result + this.number.ToString().Replace("/", ".");
                }
                else
                {
                    Result += this.currentOperator != null && this.currentOperator.TypeId != '!' ? this.currentOperator.TypeId.ToString() : "";
                    return Result + this.VariableName;
                }
            }
        }

        public class Operator
        {
            #region properties

            private char operatorId = '!';

            #endregion

            public string Type
            {
                get
                {
                    switch (this.operatorId)
                    {
                        case '^':
                            return "pow";
                        case '*':
                            return "multiplication";
                        case '/':
                            return "division";
                        case '+':
                            return "sum";
                        case '-':
                            return "subtraction";
                        default:
                            return "Unknown";
                    }
                }
            }
            public char TypeId { get { return this.operatorId; } }
            public Expression defaultExpression()
            {
                // a default expression that the opreate function takes no action
                // for example if +4 default operator of '+' should be 0, so 0+4 will be the same expression after operator (4)
                switch (this.operatorId)
                {
                    case '^':
                        return new Expression(1);
                    case '*':
                        return new Expression(1);
                    case '/':
                        return new Expression(0);
                    case '+':
                        return new Expression(0);
                    case '-':
                        return new Expression(0);
                    default:
                        return new Expression(0);
                }
            }

            public Operator(char OperatorId)
            {
                if (!Consts.Opts.Contains(OperatorId))
                {
                    throw new Exception("invalid operator, currentOperator: " + OperatorId.ToString());
                }
                this.operatorId = OperatorId;
            }

            public Operator(Operator oldOne)
            {
                if (oldOne != null)
                {
                    this.operatorId = oldOne.operatorId;
                }
            }

            public Expression Operate(Expression parent, Expression LastExpression)
            {
                Expression A = LastExpression.Calculate();
                Expression B = parent.Calculate();
                if (A.currentType == "number" && A.currentType == "number")
                {
                    switch (this.operatorId)
                    {
                        case '^':
                            return new Expression((double)(Math.Pow((double)(A.number), (double)(B.number))));
                        case '*':
                            return new Expression(A.number * B.number);
                        case '/':
                            return new Expression(A.number / B.number);
                        case '+':
                            return new Expression(A.number + B.number);
                        case '-':
                            return new Expression(A.number - B.number);
                        default:
                            return new Expression(A.number);
                    }
                }
                else
                {
                    return new Expression(new List<Expression> { A, B });
                }
            }
        }
    }
}
