﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEDatastructure0.Core
{
    class Functions
    {
        public static Dictionary<string, Func<Expression, Expression>> Funcs = new Dictionary<string, Func<Expression, Expression>>()
        {
            {"sin", new Func<Expression, Expression>(sin)},
            {"cos", new Func<Expression, Expression>(cos)},
            {"ln", new Func<Expression, Expression>(ln)}
        };

        private static Expression sin(Expression A)
        {
            if (A.isNumber)
            {
                return new Expression((double)Math.Sin((double)A.Number));
            }
            throw new Exception("Sin function accepts only numbers");
        }

        private static Expression cos(Expression A)
        {
            if (A.isNumber)
            {
                return new Expression((double)Math.Sin((double)A.Number));
            }
            throw new Exception("Cos function accepts only numbers");
        }

        private static Expression ln(Expression A)
        {
            if (A.isNumber)
            {
                return new Expression((double)Math.Log((double)A.Number, Math.E));
            }
            throw new Exception("ln function accepts only numbers");
        }
    }
}
