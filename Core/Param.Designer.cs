﻿namespace CEDatastructure0.Core
{
    partial class Param
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paramLbl = new System.Windows.Forms.Label();
            this.paramVal = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // paramLbl
            // 
            this.paramLbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.paramLbl.Font = new System.Drawing.Font("Microsoft YaHei", 24F);
            this.paramLbl.Location = new System.Drawing.Point(0, 0);
            this.paramLbl.Name = "paramLbl";
            this.paramLbl.Size = new System.Drawing.Size(57, 50);
            this.paramLbl.TabIndex = 0;
            this.paramLbl.Text = "M:";
            this.paramLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // paramVal
            // 
            this.paramVal.Dock = System.Windows.Forms.DockStyle.Right;
            this.paramVal.Font = new System.Drawing.Font("Microsoft YaHei", 24F);
            this.paramVal.Location = new System.Drawing.Point(52, 0);
            this.paramVal.Name = "paramVal";
            this.paramVal.Size = new System.Drawing.Size(223, 50);
            this.paramVal.TabIndex = 1;
            this.paramVal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.paramVal.TextChanged += new System.EventHandler(this.paramVal_TextChanged);
            // 
            // Param
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.paramVal);
            this.Controls.Add(this.paramLbl);
            this.Name = "Param";
            this.Size = new System.Drawing.Size(275, 50);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label paramLbl;
        private System.Windows.Forms.TextBox paramVal;
    }
}
