﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CEDatastructure0.Core
{
    public partial class Param : UserControl
    {
        public Param()
        {
            InitializeComponent();
        }

        private string Param_Name = "";

        public string value
        {
            get
            {
                return Convert.ToDouble(this.paramVal.Text.Replace(".", "/").Replace(" ", "")).ToString();
            }
        }

        public string paramName
        {
            get
            {
                return this.Param_Name;
            }
            set {
                this.Param_Name = value;
                this.paramLbl.Text = this.Param_Name + ":";
            }
        }

        private void paramVal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDouble(paramVal.Text.Replace(".", "/").Replace(" ", ""));
                this.paramVal.ForeColor = SystemColors.WindowText;
            }
            catch (Exception ert)
            {
                this.paramVal.ForeColor = Color.Red;
            }
        }
    }
}
