﻿namespace CEDatastructure0
{
    partial class Entry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.entry_txt_main = new System.Windows.Forms.TextBox();
            this.entry_btn_calc = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ParamList = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // entry_txt_main
            // 
            this.entry_txt_main.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.entry_txt_main.Font = new System.Drawing.Font("Microsoft YaHei", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entry_txt_main.Location = new System.Drawing.Point(14, 12);
            this.entry_txt_main.Name = "entry_txt_main";
            this.entry_txt_main.Size = new System.Drawing.Size(568, 50);
            this.entry_txt_main.TabIndex = 0;
            this.entry_txt_main.Text = "ln(NP^3)*2";
            this.entry_txt_main.TextChanged += new System.EventHandler(this.entry_txt_main_TextChanged);
            this.entry_txt_main.KeyUp += new System.Windows.Forms.KeyEventHandler(this.entry_txt_main_KeyUp);
            // 
            // entry_btn_calc
            // 
            this.entry_btn_calc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.entry_btn_calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.entry_btn_calc.Location = new System.Drawing.Point(514, 170);
            this.entry_btn_calc.Name = "entry_btn_calc";
            this.entry_btn_calc.Size = new System.Drawing.Size(98, 31);
            this.entry_btn_calc.TabIndex = 1;
            this.entry_btn_calc.Text = "Calc";
            this.entry_btn_calc.UseVisualStyleBackColor = true;
            this.entry_btn_calc.Click += new System.EventHandler(this.entry_btn_calc_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.ParamList);
            this.panel1.Controls.Add(this.entry_txt_main);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 152);
            this.panel1.TabIndex = 2;
            // 
            // ParamList
            // 
            this.ParamList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ParamList.AutoScroll = true;
            this.ParamList.Location = new System.Drawing.Point(14, 68);
            this.ParamList.Name = "ParamList";
            this.ParamList.Size = new System.Drawing.Size(568, 63);
            this.ParamList.TabIndex = 1;
            // 
            // Entry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(624, 208);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.entry_btn_calc);
            this.Name = "Entry";
            this.ShowIcon = false;
            this.Text = "CEDS0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Entry_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox entry_txt_main;
        private System.Windows.Forms.Button entry_btn_calc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel ParamList;
    }
}