﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CEDatastructure0.Core;

namespace CEDatastructure0
{
    public partial class Entry : Form
    {
        public Entry()
        {
            InitializeComponent();
        }

        Dictionary<char, Param> ParamsList = new Dictionary<char, Param>() { };

        bool showResult = false;
        private void entry_btn_calc_Click(object sender, EventArgs e)
        {
            Calc();
        }

        public void Calc()
        {
            try
            {
                Expression cuExp = new Expression();
                if (ParamsList.Count != 0)
                {
                    Dictionary<string, double> ParamsToPass = new Dictionary<string, double>() { };
                    foreach (var key in ParamsList.Keys)
                    {
                        ParamsToPass.Add(key.ToString(), Convert.ToDouble(ParamsList[key].value));
                    }
                    cuExp.Analyze(entry_txt_main.Text, ParamsToPass);
                }
                else
                {
                    cuExp.Analyze(entry_txt_main.Text, new Dictionary<string, double>() { });
                }
                Expression Ans = cuExp.Calculate();
                MessageBox.Show(this, "Result: " + Ans.ToString());
            }
            catch (Exception ert)
            {
                MessageBox.Show(this, ert.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }

        private void entry_txt_main_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                showResult = !showResult;
                if (showResult)
                    Calc();
            }
        }

        private void entry_txt_main_TextChanged(object sender, EventArgs e)
        {
            List<char> allVariables = new List<char>(extract_params(entry_txt_main.Text));
            List<char> currentParamsKeyList = ParamsList.Keys.ToList<char>();
            for (int i = 0; i < allVariables.Count; i++)
            {
                if (currentParamsKeyList.Contains(allVariables[i]))
                {
                    currentParamsKeyList.Remove(allVariables[i]);
                    allVariables.Remove(allVariables[i]);
                }
            }
            // chars in allVariable list should be created, and chars in currentParamsKeyList should be removed from UI
            foreach (char removingItems in currentParamsKeyList)
            {
                ParamList.Controls.Remove(ParamsList[removingItems]);
                ParamsList.Remove(removingItems);
            }
            foreach(char addingItem in allVariables)
            {
                Param newParam = new Param();
                ParamsList.Add(addingItem, newParam);
                newParam.paramName = addingItem.ToString();
                ParamList.Controls.Add(newParam);
            }
        }


        private char[] extract_params(string Raw)
        {
            List<char> result = new List<char>() { };
            foreach (var singleStr in Consts.Constants.Keys)
            {
                Raw = Raw.Replace(singleStr, "");
            }
            foreach (var singleStr in Functions.Funcs.Keys)
            {
                Raw = Raw.Replace(singleStr, "");
            }
            foreach (char varChar in Consts.Vars)
            {
                if (Raw.Contains(varChar))
                {
                    Raw = Raw.Replace(varChar.ToString(), "");
                    if (!result.Contains(varChar))
                    {
                        result.Add(varChar);
                    }
                }
            }
            return result.ToArray();
        }

        private void Entry_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageBox.Show(this, "2019, October, Tabriz university\nFaculty of Electrical and Computer Engineering\nData Structure\nProject0: Scientific Calculator\n\nMahdi Naji\nID:975367026", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}
